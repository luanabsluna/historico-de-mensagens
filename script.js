
const sendButton = document.getElementById('enviar');
var cont = 0;
var isEdit = false;
var idMessageEdit = ""

function createBoxMessage(text){
    const newBoxMessage = document.createElement("div");
    newBoxMessage.className="box-mensagem";
    newBoxMessage.id=`box-mensagem${cont}`

    const newTextarea = document.createElement("textarea");
    newTextarea.className="view-message";
    newTextarea.id=`view-message${cont}`
    newTextarea.readOnly=true;
    newTextarea.value = text;

    const newBoxMessageButtons = document.createElement("div");
    newBoxMessageButtons.className="box-mensagem-buttons"

    const newEditButton =  document.createElement("button");
    newEditButton.className="edit-button";
    newEditButton.id=`edit${cont}`
    newEditButton.textContent="Editar";

    const newDeleteButton =  document.createElement("button");
    newDeleteButton.className="delete-button";
    newDeleteButton.id=`delete${cont}`
    newDeleteButton.textContent="Excluir";

    const areaMessage = document.getElementById("area-mensagens"); // pegou a area de mensagem

    newBoxMessageButtons.insertBefore(newEditButton, null); // inseriu o botao de editar na div box-mensagem-buttons 
    newBoxMessageButtons.insertBefore(newDeleteButton, null); // inseriu o botao de excluir na div box-mensagem-buttons 

    newBoxMessage.insertBefore(newTextarea, null); // inseriu a caixa de texto na div box-mensagem 
    newBoxMessage.insertBefore(newBoxMessageButtons, null); // inseriu os botoes na div box-mensagem 

    areaMessage.insertBefore(newBoxMessage, null); // inseriu a mensagem nova no html no final

    newDeleteButton.onclick = function() {
        newBoxMessage.remove();
    }

    newEditButton.onclick = function() {
        isEdit=true;
        idMessageEdit = newTextarea.id;
        document.getElementById('write-message').value = newTextarea.value;
    }
    cont++;

}

sendButton.onclick = function() {
    const text = document.getElementById('write-message').value;
    if(isEdit){
        isEdit=false;
        document.getElementById(idMessageEdit).value = text;
    }
    else{
        createBoxMessage(text);
    }
    document.getElementById('write-message').value="";
}




